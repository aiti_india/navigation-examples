package edu.mit.aiti.india.navexamples;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.support.v4.app.NavUtils;

public class NavigationExamples extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_examples);
        
        ListView myListView =
        		(ListView) findViewById(R.id.myListView);
        ArrayAdapter<String> stateList = new ArrayAdapter<String>(
        		this,
        		android.R.layout.simple_list_item_1,
        		getResources().getStringArray(R.array.states));
        myListView.setAdapter(stateList);
        myListView.setOnItemClickListener(new OnItemClickListener() {
        	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            	/* Create the intent to start AnotherActivity from this activity. */
            	Intent anotherActivityIntent =
            			new Intent(
            					NavigationExamples.this,
            					AnotherActivity.class);

            	startActivity(anotherActivityIntent);
        	}
        });
        registerForContextMenu(myListView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_navigation_examples, menu);
        return true;
    }
    
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
    	case R.id.menu_settings:
    		/* Do something with the settings menu item. */
    		return true;
    	case R.id.menuItem:
        	/* Create the intent to start AnotherActivity from this activity. */
        	Intent anotherActivityIntent =
        			new Intent(
        					NavigationExamples.this,
        					AnotherActivity.class);

        	startActivity(anotherActivityIntent);
        	return true;
        default:
        	return super.onOptionsItemSelected(item);
    	}
    }
    
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenuInfo info) {
    	super.onCreateContextMenu(menu, view, info);

    	MenuInflater inflater = getMenuInflater();
    	inflater.inflate(R.menu.activity_navigation_examples, menu);
    }
    
    public boolean onContextItemSelected(MenuItem item) {
    	Activity thisActivity = this;
    	
    	switch (item.getItemId()) {
    	case R.id.menu_settings:
    		return true;
    	case R.id.menuItem:
    		/* Get the NotificationManager service. */
    		String ns = Context.NOTIFICATION_SERVICE;
    		NotificationManager mNotificationManager = (NotificationManager) getSystemService(ns);
    		
    		/* Create the notification. */
    		int icon = R.drawable.ic_launcher;            // Icon
    		CharSequence tickerText = "The ticker text";  // Ticker Text
    		long when = System.currentTimeMillis();       // When to show the notification

    		Notification notification = new Notification(icon, tickerText, when);
    		
    		/* Prepare the event info to send to the NotificationManager (the message) */
    		Context context = getApplicationContext();
    		CharSequence contentTitle = "My notification";  // The title of the notification
    		CharSequence contentText = "Hello World!";      // The text
    		/* This intent is "delayed" to start up this activity at a later date. */
    		Intent notificationIntent = new Intent(thisActivity, NavigationExamples.class);
    		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

    		notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);
    		
    		/* Send the notification. */
    		final int HELLO_ID = 1;

    		mNotificationManager.notify(HELLO_ID, notification);
    		return true;
    	default:
    		return super.onContextItemSelected(item);
    	}
    }
}
